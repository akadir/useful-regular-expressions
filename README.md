# Useful Regular Expressions

- Match text between strings
  
  ```
  (?<=START_STRING).*(?=END_STRING)
  ```

- Validate e-mail address considering Turkish characters

  ```
  /^(([^<>()\[\]\\.,;:\s@"][^şŞçÇİıöÖüÜğĞüÜ]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))[^şŞçÇİıöÖüÜğĞüÜ]@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9][^şŞçÇİıöÖüÜğĞüÜ]+\.)+[a-zA-Z][^şŞçÇİıöÖüÜğĞüÜ]{2,}))$/; 
  ```